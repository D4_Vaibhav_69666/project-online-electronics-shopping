CREATE TABLE cart (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  quantity int NOT NULL,
  product_id int,
  user_id int,
  FOREIGN KEY (product_id) REFERENCES product(id),
  FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE user (
  id int NOT NULL AUTO_INCREMENT,
  email_id varchar(255),
  first_name varchar(255),
  last_name varchar(255),
  password varchar(255),
  phone_no varchar(255),
  role varchar(255),
  address_id int,
  PRIMARY KEY (id),
  KEY address_fk (address_id),
  CONSTRAINT address_fk
    FOREIGN KEY (address_id)
    REFERENCES address(id)
    ON DELETE SET NULL
);

CREATE TABLE address (
  id int NOT NULL AUTO_INCREMENT,
  city varchar(255),
  pincode int NOT NULL,
  street varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE category (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  description VARCHAR(255),
  title VARCHAR(255)
);


CREATE TABLE orders (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  delivery_assigned VARCHAR(255),
  delivery_date VARCHAR(255),
  delivery_person_id INT NOT NULL,
  delivery_status VARCHAR(255),
  delivery_time VARCHAR(255),
  order_date VARCHAR(255),
  order_id VARCHAR(255),
  quantity INT NOT NULL,
  product_id INT,
  user_id INT,
  FOREIGN KEY (product_id) REFERENCES product(id),
  FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE product (
    id INT NOT NULL AUTO_INCREMENT,
    description VARCHAR(255),
    image_name VARCHAR(255),
    price DECIMAL(19,2),
    quantity INT NOT NULL,
    title VARCHAR(255),
    category_id INT,
    PRIMARY KEY (id),
    CONSTRAINT fk_category
        FOREIGN KEY (category_id)
        REFERENCES category(id)
        ON UPDATE CASCADE
        ON DELETE SET NULL
);

CREATE TABLE review (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  comment VARCHAR(255) NOT NULL,
  rating DOUBLE NOT NULL,
  product_id INT,
  user_id INT,
  FOREIGN KEY (product_id) REFERENCES product(id),
  FOREIGN KEY (user_id) REFERENCES user(id)
);


